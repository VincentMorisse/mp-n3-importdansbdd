import unittest, csv, os, shutil, sqlite3 

from importBDD import ExtensionCSVisGood, ExtensionSQLITE3isGood, csvToList, verifColumn, insertData, updateData

class ImportBDDTestCase(unittest.TestCase):

    def setUp(self):
        """
        Creation of the Data Base
        """
        self.connection = sqlite3.connect('BDD_test.sqlite3')
        cursor = self.connection.cursor()
        cursor.execute('''CREATE TABLE IF NOT EXISTS SIV(adresse_titulaire text,nom text,prenom text,immatriculation text,date_immatriculation text,vin text ,marque text, denomination_commerciale text, couleur text, carrosserie text,categorie text ,cylindree text, energie text, places text , poids text , puissance text , type text , variante text , version text)''')
        cursor.close()

    def tearDown(self):
        """
        Deletion of the Data Base
        """
        self.connection.close()

    def test_ExtensionCSVisGood(self):
        self.assertTrue(ExtensionCSVisGood("file.csv"))
        self.assertFalse(ExtensionCSVisGood("file.txt"))

    def test_ExtensionSQLITE3isGood(self):
        self.assertTrue(ExtensionSQLITE3isGood("file.sqlite3"))
        self.assertFalse(ExtensionSQLITE3isGood("file.csv"))

    def test_csvToList(self):
        self.assertListEqual([['3822 Omar Square Suite 257 Port Emily, OK 43251', 'Smith', 'Jerome', 'OVC-568', '03/05/2012', '9780082351764', 'Williams Inc', 'Enhanced well-modulated moderator', 'LightGoldenRodYellow', '45-1743376', '34-7904216', '3462', '37578077', '32', '3827', '110', 'Inc', ' 92-3625175', ' 79266482']], csvToList("tests/test.csv", ';'))

    def test_verifColumn(self):
        self.assertTrue(verifColumn("tests/test.csv", ';'))
        self.assertFalse(verifColumn("tests/falsetest.csv", ';'))

    def test_insertData(self):

        listdatatest=['3822 Omar Square Suite 257 Port Emily, OK 43251', 'Smith', 'Jerome', 'OVC-568', '03/05/2012', '9780082351764', 'Williams Inc', 'Enhanced well-modulated moderator', 'LightGoldenRodYellow', '45-1743376', '34-7904216', '3462', '37578077', '32', '3827', '110', 'Inc', ' 92-3625175', ' 79266482']
        insertData(listdatatest, self.connection)

        cursor=self.connection.cursor()
        result = cursor.execute("SELECT COUNT(*) FROM SIV").fetchone()
        self.assertEqual(1, result[0])
        cursor.close()

    def test_updateData(self):
        cursor = self.connection.cursor()

        listdatatest=['3822 Omar Square Suite 257 Port Emily, OK 43251', 'Smith', 'Jerome', 'OVC-568', '03/05/2012', '9780082351764', 'Williams Inc', 'Enhanced well-modulated moderator', 'LightGoldenRodYellow', '45-1743376', '34-7904216', '3462', '37578077', '32', '3827', '110', 'Inc', ' 92-3625175', ' 79266482']
        insertData(listdatatest, self.connection)

        listdataupdate=['Je suis différent, OK 43251', 'Smith', 'Jerome', 'OVC-568', '03/05/2012', '9780082351764', 'Williams Inc', 'Enhanced well-modulated moderator', 'LightGoldenRodYellow', '45-1743376', '34-7904216', '3462', '37578077', '32', '3827', '110', 'Inc', '92-3625175', '79266482']
        updateData(listdataupdate,self.connection)

        resultat = cursor.execute("SELECT * FROM SIV").fetchone()

        self.assertEqual(listdataupdate[0], resultat[0])
    
