import csv, sys, sqlite3, logging

logging.basicConfig(filename='example.log',format='%(asctime)s:%(levelname)s:%(message)s',level=logging.DEBUG)

def ExtensionCSVisGood(file):
    '''
    Verification de l'extension csv
    return bool
    '''
    if file[-3:] == 'csv' :
        logging.info('Le fichier fournit est bien au bon format')
        return True
    else:
        logging.error('Le fichier fournit n\'est pas au bon format, le programme ne peut donc pas s\'executer')
        return False

def ExtensionSQLITE3isGood(file):
    '''
    Verification de l'extension sqlite3
    return bool
    '''
    if file[-7:] == 'sqlite3' :
        logging.info('La BDD fournit est bien au bon format')
        return True
    else:
        logging.error('La BDD fournit n\'est pas au bon format, le programme ne peut donc pas s\'executer')
        return False

def csvToList(file, delimit):
    '''
    Met les données du csv donnée en parametre dans une liste, avec le delimiter
    return list
    '''
    dataList = []
    compteur = 0
    with open(file,'r') as csvfile:   
        reader=csv.reader(csvfile,delimiter=delimit)
        for lines in reader:
            if compteur != 0:
                dataList.append(lines)
            compteur += 1
    logging.info("Donnee transferees dans une liste")
    return dataList

def verifColumn(file, delimit):
    '''
    Verifie si les colonnes du csv correspondent au colonnes attenduent dans la BDD
    return bool
    '''
    column = ['adresse_titulaire', 'nom', 'prenom', 'immatriculation', 'date_immatriculation', 'vin', 'marque', 'denomination_commerciale', 'couleur', 'carrosserie', 'categorie', 'cylindree', 'energie', 'places', 'poids', 'puissance', 'type', 'variante', 'version']
    with open(file,'r') as csvfile:   
        reader=csv.reader(csvfile,delimiter=delimit)
        if column in reader:
            return True
            logging.info('Les colonnes de la BDD correspondent au fichier fournit')
        else:
            return False
            logging.error('Les colonnes de la BDD ne correspondent pas au fichier fournit')

def presenceTable(bddpath):
    '''
    Verifie la presence de la table dans la BDD sinon elle est créée
    '''
    connection = sqlite3.connect(bddpath)
    cursor = connection.cursor()
    cursor.execute('''CREATE TABLE IF NOT EXISTS SIV(adresse_titulaire text,nom text,prenom text,immatriculation text,date_immatriculation text,vin text ,marque text, denomination_commerciale text, couleur text, carrosserie text,categorie text ,cylindree text, energie text, places text , poids text , puissance text , type text , variante text , version text)''')
    logging.info('Creation de la table SIV si elle n\'existe pas')

def insertData(dataList, connection):
    '''
    Importe des données de dataList dans la bdd
    '''
    cursor = connection.cursor()
    cursor.execute('INSERT INTO SIV(adresse_titulaire,nom,prenom,immatriculation,date_immatriculation,vin ,marque, denomination_commerciale , couleur, carrosserie ,categorie ,cylindree , energie , places  , poids , puissance , type , variante , version ) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',dataList)
    logging.info('Insertion de %s' %(dataList[3]))
    cursor.close()

def moveImmat(dataList):
    '''
    Place l'immatriculation de la list en derniere position
    '''
    immat=dataList.pop(3)
    dataList.append(immat)
    return dataList

def updateData(dataList, connection):
    '''
    Met a jour les données par rapport à l'immatriculation
    '''
    cursor = connection.cursor()
    cursor.execute("UPDATE SIV set adresse_titulaire = ?, nom = ?, prenom = ?, date_immatriculation = ?,vin =?,  marque = ?, denomination_commerciale = ?, couleur = ?, carrosserie = ?, categorie = ?, cylindree = ?, energie = ?, places = ?, poids = ?, puissance = ?, type = ?, variante = ?, version = ? WHERE siv.immatriculation=?", moveImmat(dataList))
    logging.info('Mise a jour de %s' %(dataList[18]))
    cursor.close()

def remplissage(dataList, connection):
    '''
    Determine si il faut mettre a jour ou inserer une données dans la table siv, puis execute l'import ou la maj
    '''
    cursor = connection.cursor()
    for row in dataList:
        insert=True
        for line in cursor.execute('SELECT immatriculation FROM siv '):
            if row[3]==line[0]:
                insert=False
        if insert:
            insertData(row, connection)
        else :
            updateData(row, connection)
    cursor.close()

if __name__ == "__main__":
    if len(sys.argv)!=4 or not ExtensionCSVisGood(sys.argv[1]) or not ExtensionSQLITE3isGood(sys.argv[2]):
        print("Saisir des arguments valide (CSVpath BDDpath Delimiter)")
        logging.error("Les arguments fournient ne sont pas valides")
    else:
        if(verifColumn(sys.argv[1], sys.argv[3])):
            connection = sqlite3.connect(sys.argv[2])
            dataList = csvToList(sys.argv[1], sys.argv[3])
            presenceTable(sys.argv[2])
            remplissage(dataList, connection)
            connection.commit()
        else:
            print('Le Fichier fournit ne contient pas les colonnes attendues')
            logging.error("Le Fichier fournit ne contient pas les colonnes attendues")